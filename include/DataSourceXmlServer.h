//
// DataSourceXmlServer.h
//
// $Id: DataSourceXmlServer.h 236 2010-10-19 18:41:15Z olchansk $
//

#include "DataSourceBase.h"

#include "mxml.h"

#include <stdint.h>

#include "TBufferXML.h"

extern "C" {
//#include "http-tiny-1.2/http_lib.h"
#include "http-tiny-1.2/http_lib.c"
}

#if 0
#include <sys/time.h>

static double xtime()
{
   struct timeval tv;
   gettimeofday(&tv, NULL);
   return tv.tv_sec + 1.0e-6*tv.tv_usec;
}
#endif

class DataSourceXmlServer: public DataSourceBase
{
 private:
  std::string fBaseUrl;
  int fDebug;

 public:
  DataSourceXmlServer(const char* url) // ctor
    {
      fName = url;
      fBaseUrl = url;
      fDebug = 0;
    }

  ~DataSourceXmlServer() // dtor
    {
    }

  static std::string UrlEncode(const char* s)
     {
        std::string r;
        while (*s) {
           char c = *s;
           if (isalpha(c)) {
              r += c;
           } else if (isdigit(c)) {
              r += c;
           } else {
              char buf[16];
              sprintf(buf, "%%%02x", c);
              r += buf;
           }
           s++;
        }
        return r;
     }

  static std::string FetchUrl(const std::string& url)
  {
     printf("Fetch [%s]\n", url.c_str());
     char* data = NULL;
     int length = 0;
     char typebuf[256];
     char xurl[256];
     strcpy(xurl, url.c_str());
     char *filename = NULL;
     http_retcode rc = http_parse_url(xurl, &filename);
     if (rc != 0) {
        printf("Error: http_parse_url rc %d\n", rc);
        return "";
     }
     rc = http_get(filename, &data, &length, typebuf);
     if (rc != OK200) {
        printf("Error: http_get rc %d\n", rc);
        return "";
     }
     //printf("data %p, length %d, typebuf [%s]\n", data, length, typebuf);
     //printf("actual length %d\n", strlen(data));
     //if (strlen(data) > length)
     //   data[length] = 0;
     assert((int)strlen(data) <= length);
     std::string xdata = data;
     if (filename)
        free(filename);
     if (data)
        free(data);
     return xdata;
  }

#if 0
  static void ExecuteRemoteCommand(TSocket* socket, const char *line)
    {
      if (!socket->IsValid())
	return;
      
      // The line is executed by the CINT of the server
      socket->Send("Execute");
      socket->Send(line);
    }
#endif

  void Enumerate(ObjectList* list, const std::string& urlpath, int level, ObjectPath path)
  {
     if (fDebug)
        printf("Enumerate [%s] level %d\n", urlpath.c_str(), level);
    std::string xmltext;
    if (level == 0)
       xmltext = FetchUrl(urlpath + "/index.xml");
    else
       xmltext = FetchUrl(urlpath + ".xml");
    if (xmltext.length() < 1)
       return;
    //printf("xmltext [%s]\n", xmltext.c_str());
    char error[256];
    int errline = 0;
    PMXML_NODE tree = mxml_parse_buffer(xmltext.c_str(), error, strlen(error), &errline);
    if (tree) {
       //mxml_write_tree("test.xml", tree);

       PMXML_NODE dir = mxml_find_node(tree, "xml/dir");
       int num_entries = 0;
       if (dir)
          num_entries = mxml_get_number_of_children(dir);
       for (int i=0; i<num_entries; i++) {
          PMXML_NODE node = mxml_subnode(dir, i);
          if (node) {
             //printf("subnode %d %p\n", i, node);
             const char* ntype     = mxml_get_name(node);
             const char* classname = mxml_get_value(mxml_find_node(node, "class"));
             const char* name      = mxml_get_value(mxml_find_node(node, "name"));
             
             if (fDebug)
                printf("node type %s, class %s, name %s\n", ntype, classname, name);
             
             if (strcmp(ntype, "subdir")==0) {
                ObjectPath p = path;
                p.push_back(name);
                Enumerate(list, urlpath + "/" + UrlEncode(name), level+1, p);
             } else {
                ObjectPath p = path;
                std::string s;
                s += name;
                //s += "(";
                //s += classname;
                //s += ")";
                p.push_back(s);
                list->push_back(p);
             }
          }
       }
          
       mxml_free_tree(tree);
    }
  }

  ObjectList GetList()
  {
    printf("GetList from %s\n", fName.c_str());
    ObjectList list;
    ObjectPath path;
    path.push_back(fBaseUrl);
    Enumerate(&list, fBaseUrl, 0, path);
    return list;
  }

  static std::string ObjectPathToUrl(const ObjectPath& path)
     {
        if (path.size() < 1)
           return "(empty)";
        std::string s;
        s += path.front();
        for (unsigned int i=1; i<path.size(); i++)
           {
              s += "/";
              s += UrlEncode(path[i].c_str());
           }
        return s;
     }

  TObject* GetObject(const ObjectPath& path)
    {
      TObject *obj = NULL;
      //const char *name = path.back().c_str();

      if (fDebug)
         printf("Get object %s from %s\n", path.toString().c_str(), fName.c_str());

      //double t1 = xtime();

      std::string xmltext = FetchUrl(ObjectPathToUrl(path) + ".xml");
      //double t2 = xtime();
      if (xmltext.length() > 0) {
         //printf("xmltext [%s...]\n", xmltext.substr(0,50).c_str());

         if (xmltext.substr(0,5) == "<?xml") {
            // this is XML

            if (strstr(xmltext.c_str(), "ROOTobject")) {
               const char *s = strstr(xmltext.c_str(), "<Object");
               if (s) {
                  //printf("parse %s\n", s);
                  //double t3 = xtime();
                  //double t4 = xtime();
                  obj = TBufferXML::ConvertFromXML(s);
                  //double t5 = xtime();
                  //printf("time to load %f. Fetch %f, parse %f, find %f, convert %f\n", t5-t1, t2-t1, t3-t2, t4-t3, t5-t4);
                  //printf("obj %p\n", obj);
                  if (fDebug && obj) {
                     printf("Got object: ");
                     obj->Print();
                  }
               }
            }
         }
      }
      return obj;
    }

  void ResetAll()
  {
    printf("ResetAll %s: not implemented\n", fName.c_str());
  }

  void ResetObject(const ObjectPath& path)
  {
    printf("ResetObject %s: not implemented\n", path.toString().c_str());
  }

};

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
